﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace dogs
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            database db_connector = new database();
            DataTable dataTable = new DataTable();
            DataTable dataTable1 = new DataTable();
            DataTable dataTable2 = new DataTable();
            MySqlDataAdapter dataAdapter = new MySqlDataAdapter();
            MySqlDataAdapter dataAdapter1 = new MySqlDataAdapter();
            MySqlDataAdapter dataAdapter2 = new MySqlDataAdapter();

            

            if (textBox_name.TextLength > 1)
            {
                try
                {
                    string command_string = "SELECT * from `users` WHERE `login`=@login AND `password`=@password AND role = 0";

                    MySqlCommand sqlCommand = new MySqlCommand(command_string, db_connector.GetConnection());
                    sqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_name.Text;
                    sqlCommand.Parameters.Add("@password", MySqlDbType.VarChar).Value = textBox1.Text;

                    dataAdapter.SelectCommand = sqlCommand;
                    dataAdapter.Fill(dataTable);

                    if (dataTable.Rows.Count < 1)
                    {
                        string command_string1 = "SELECT * from `users` WHERE `login`=@login AND `password`=@password AND role = 1";

                        MySqlCommand sqlCommand1 = new MySqlCommand(command_string1, db_connector.GetConnection());
                        sqlCommand1.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_name.Text;
                        sqlCommand1.Parameters.Add("@password", MySqlDbType.VarChar).Value = textBox1.Text;

                        dataAdapter1.SelectCommand = sqlCommand1;
                        dataAdapter1.Fill(dataTable1);

                        if (dataTable1.Rows.Count < 1)
                        {
                            string command_string2 = "SELECT * from `users` WHERE `login`=@login AND `password`=@password AND role = 2";

                            MySqlCommand sqlCommand2 = new MySqlCommand(command_string2, db_connector.GetConnection());
                            sqlCommand2.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_name.Text;
                            sqlCommand2.Parameters.Add("@password", MySqlDbType.VarChar).Value = textBox1.Text;

                            dataAdapter2.SelectCommand = sqlCommand2;
                            dataAdapter2.Fill(dataTable2);

                            if (dataTable2.Rows.Count > 1)
                                MessageBox.Show("Проверьте введеность данных, если считаете что все нормально - обратитесь к администратору системы", "Ошибка!");
                            else
                            {
                                admin_panel _Panel1 = new admin_panel();
                                _Panel1.Show();
                                this.Hide();
                            }
                        } else
                        {
                            kin_panel _Panel1 = new kin_panel();
                            _Panel1.Show();
                            this.Hide();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Успешно", "Информация");
               
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    //MessageBox.Show("Ошибка подключения к базе днных.", "Информация");
                }
                finally
                {
                    db_connector.CloseConnection();
                }

            }
        }

    }
}
