﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
namespace dogs
{
    public partial class kin_panel : Form
    {
        public kin_panel()
        {
            InitializeComponent();
			loadData1();
			loadData2();
			loadData3();
			loadData4();
        }

		public void loadData1()
        {
			database db = new database();

			string string_command = "SELECT * from `dogs`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView1.Columns.Add("ID_Book", "Номер членской книжки");
				dataGridView1.Columns.Add("ID_Breed", "Разводчик");
				dataGridView1.Columns.Add("ID_pass", "мед. Паспорт");
				dataGridView1.Columns.Add("ID_mark_dog", "Порода");
				dataGridView1.Columns.Add("Breeder", "Разводчик");
				dataGridView1.Columns.Add("Name_dog", "Кличка");
				dataGridView1.Columns.Add("Year_dog", "Дата рождения");
				while (mySqlDataReader.Read())
				{
					dataGridView1.Rows.Add(mySqlDataReader["ID_Book"].ToString(), mySqlDataReader["ID_Breed"].ToString(), mySqlDataReader["ID_pass"].ToString(), mySqlDataReader["ID_mark_dog"].ToString(), mySqlDataReader["Breeder"].ToString(), mySqlDataReader["Name_dog"].ToString(), mySqlDataReader["Year_dog"].ToString());
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData2()
		{
			database db = new database();

			string string_command = "SELECT * from `med_passport_dog`,`type_string`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView2.Columns.Add("ID_pass", "Номер записи");
				dataGridView2.Columns.Add("String", "Процедура"); 
				while (mySqlDataReader.Read())
				{
					dataGridView2.Rows.Add(mySqlDataReader["ID_pass"].ToString(), mySqlDataReader["String"].ToString());
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData3()
		{
			database db = new database();

			string string_command = "SELECT * from `dogs`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView3.Columns.Add("ID_Book", "Номер членской книжки");
				dataGridView3.Columns.Add("ID_Breed", "Разводчик");
				dataGridView3.Columns.Add("ID_pass", "мед. Паспорт");
				dataGridView3.Columns.Add("ID_mark_dog", "Порода");
				dataGridView3.Columns.Add("Breeder", "Разводчик");
				dataGridView3.Columns.Add("Name_dog", "Кличка");
				dataGridView3.Columns.Add("Year_dog", "Дата рождения");
				while (mySqlDataReader.Read())
				{
					dataGridView3.Rows.Add(mySqlDataReader["ID_Book"].ToString(), mySqlDataReader["ID_Breed"].ToString(), mySqlDataReader["ID_pass"].ToString(), mySqlDataReader["ID_mark_dog"].ToString(), mySqlDataReader["Breeder"].ToString(), mySqlDataReader["Name_dog"].ToString(), mySqlDataReader["Year_dog"].ToString());
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData4()
		{
			database db = new database();

			string string_command = "SELECT * from `in_races`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView4.Columns.Add("Number_r", "Номер заезда");
				dataGridView4.Columns.Add("ID_mark_dog", "Номер клички");
				dataGridView4.Columns.Add("ID_race", "Номер заезда");
				dataGridView4.Columns.Add("ID_team", "Разводчик");
				dataGridView4.Columns.Add("ID_sied", "Кличка");
				while (mySqlDataReader.Read())
				{
					dataGridView4.Rows.Add(mySqlDataReader["Number_r"].ToString(),  mySqlDataReader["ID_race"].ToString(), mySqlDataReader["ID_mark_dog"].ToString(), mySqlDataReader["ID_team"].ToString(), mySqlDataReader["ID_sied"].ToString());
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button5_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox1.SelectedItem == "Хаски")
				id = 1;
			if (listBox1.SelectedItem == "Маламут")
				id = 2;
			if (listBox1.SelectedItem == "Самоедская лайка")
				id = 3;
			if (listBox1.SelectedItem == "Сибо ино")
				id = 4;
			if (listBox1.SelectedItem == "Гренландская ездовая собака")
				id = 5;
			if (listBox1.SelectedItem == "Норвежский спортивный метис")
				id = 6;
			if (listBox1.SelectedItem == "Еврохаунд")
				id = 7;
			if (listBox1.SelectedItem == "Другая")
				id = 8;

			database db = new database();

			string command_string = "INSERT INTO `dogs` (ID_Book, ID_Breed, ID_pass , ID_mark_dog, Breeder, Name_dog, Year_dog) VALUES (@ID_Book, @ID_Breed,@ID_pass,@ID_mark_dog,@Breeder,@Name_dog,@Year_dog)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@ID_Book", MySqlDbType.Int32).Value = textBox_id.Text;
			mySqlCommand.Parameters.Add("@ID_Breed", MySqlDbType.Int32).Value = textBox_num.Text;
			mySqlCommand.Parameters.Add("@ID_pass", MySqlDbType.Int32).Value = textBox_passport;
			mySqlCommand.Parameters.Add("@ID_mark_dog", MySqlDbType.Int32).Value = id;
			mySqlCommand.Parameters.Add("@Breeder", MySqlDbType.VarChar).Value = breed.Text;
			mySqlCommand.Parameters.Add("@Name_dog", MySqlDbType.VarChar).Value = textBox_name.Text;
			mySqlCommand.Parameters.Add("@Year_dog", MySqlDbType.DateTime).Value = dateTimePicker1.Value;

			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Собака успешно добавлена", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button7_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox2.SelectedItem == "Прививка")
				id = 1;
			if (listBox2.SelectedItem == "Осмотр молодняка")
				id = 2;
			if (listBox2.SelectedItem == "Травма общая")
				id = 3;
			if (listBox2.SelectedItem == "Травма конечности")
				id = 4;
			if (listBox2.SelectedItem == "Инфекционное заболевание")
				id = 5;
			if (listBox2.SelectedItem == "Щенность")
				id = 6;
			if (listBox2.SelectedItem == "Вязка")
				id = 7;
			if (listBox2.SelectedItem == "Биометрия")
				id = 8;
			if (listBox2.SelectedItem == "Операционное вмещательство")
				id = 9;

			database db = new database();

			string command_string = "INSERT INTO `med_passport_dog` (ID_pass, ID_string) VALUES (@ID_pass, @ID_string)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@ID_pass", MySqlDbType.Int32).Value = textBox9.Text;
			mySqlCommand.Parameters.Add("@ID_string", MySqlDbType.Int32).Value = id;

			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Мед.запись успешно добавлена", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button8_Click(object sender, EventArgs e)
        {
			int id = 0;
			if (listBox3.SelectedItem == "Чемпион России")
				id = 1;
			if (listBox3.SelectedItem == "Юный чемпион России")
				id = 2;
			if (listBox3.SelectedItem == "Чемпион федерации")
				id = 3;
			if (listBox3.SelectedItem == "Чемпион российской кинологической федерации")
				id = 4;
			if (listBox3.SelectedItem == "Гран чемпион России")
				id = 5;
			if (listBox3.SelectedItem == "Чемпион национального кубка")
				id = 6;
			if (listBox3.SelectedItem == "Юный чемпион национального кубка")
				id = 7;
			if (listBox3.SelectedItem == "Интернациональный чемпион")
				id = 8;
			if (listBox3.SelectedItem == "Мультичемпион")
				id = 9;
			if (listBox3.SelectedItem == "Чемпион пароды года")
				id = 10;
			if (listBox3.SelectedItem == "Собака года")
				id = 11;
			if (listBox3.SelectedItem == "Элита")
				id = 12;

			database db = new database();

			string command_string = "INSERT INTO `dog_rewards` (ID_mark_dog, ID_totaly,ID_race,Награда) VALUES (@ID_mark_dog, @ID_totaly,@ID_race,@award)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@ID_mark_dog", MySqlDbType.Int32).Value = textBox11.Text;
			mySqlCommand.Parameters.Add("@ID_totaly", MySqlDbType.Int32).Value = id;
			mySqlCommand.Parameters.Add("@ID_race", MySqlDbType.Int32).Value = textBox8.Text;
			mySqlCommand.Parameters.Add("@award", MySqlDbType.VarChar).Value = textBox1.Text;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Мед.запись успешно добавлена", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

        private void button9_Click(object sender, EventArgs e)
        {
			int type_san = 0;
			if (listBox6.SelectedItem == "Нарты - 60")
				type_san = 1;
			if (listBox6.SelectedItem == "Нарты - 120")
				type_san = 2;
			if (listBox6.SelectedItem == "Нарты - 240")
				type_san = 3;
			if (listBox6.SelectedItem == "Сани - 200")
				type_san = 4;
			if (listBox6.SelectedItem == "Сани - 100")
				type_san = 5;
			if (listBox6.SelectedItem == "Карт - 150")
				type_san = 6;
			if (listBox6.SelectedItem == "Карт - 180")
				type_san = 7;
			if (listBox6.SelectedItem == "Скутер - 90")
				type_san = 8;
			if (listBox6.SelectedItem == "Байк - 90")
				type_san = 9;
			if (listBox6.SelectedItem == "Лыжи - 90")
				type_san = 10;

			int type_upr = 0;
			if (listBox4.SelectedItem == "1 собака")
				type_upr = 1;
			if (listBox4.SelectedItem == "2 собаки")
				type_upr = 2;
			if (listBox4.SelectedItem == "4 собки")
				type_upr = 3;
			if (listBox4.SelectedItem == "6 собак")
				type_upr = 4;
			if (listBox4.SelectedItem == "8 собак")
				type_upr = 5;
			if (listBox4.SelectedItem == "10 собак")
				type_upr = 6;

			database db = new database();

			string command_string = "INSERT INTO `in_races` (Number_r, ID_mark_dog,ID_race,ID_team,ID_sied) VALUES (@Number_r, @ID_mark_dog,@ID_race,@ID_team,@ID_sied)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@Number_r", MySqlDbType.Int32).Value = textBox2.Text;
			mySqlCommand.Parameters.Add("@ID_mark_dog", MySqlDbType.Int32).Value = textBox10.Text;
			mySqlCommand.Parameters.Add("@ID_race", MySqlDbType.Int32).Value = textBox12.Text;
			mySqlCommand.Parameters.Add("@ID_team", MySqlDbType.Int32).Value = type_upr;
			mySqlCommand.Parameters.Add("@ID_sied", MySqlDbType.Int32).Value = type_san;

			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Мед.запись успешно добавлена", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}
    }
}
