﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dogs
{
    public partial class admin_panel : Form
    {
       

		public admin_panel()
		{
			InitializeComponent();
			loadData();
			loadData2();
			loadData3();
			loadData4();
		}

		public void loadData()
		{
			database db = new database();
			string string_command = "SELECT * FROM `payment`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView1.Columns.Add("ID_Book", "Номер членской книжки");
				dataGridView1.Columns.Add("Account_number", "Номер счета");
				dataGridView1.Columns.Add("Data_p", "Дата оплаты");
				dataGridView1.Columns.Add("Moon", "Месяц");
				dataGridView1.Columns.Add("Summa", "Сумма");
				dataGridView1.Columns.Add("App_Not", "Статус оплаты");
				dataGridView1.Columns["ID_Book"].Width = 100;
				dataGridView1.Columns["ID_Book"].ReadOnly = true;
				dataGridView1.Columns["Account_number"].ReadOnly = true;
				dataGridView1.Columns["Data_p"].ReadOnly = true;
				dataGridView1.Columns["Moon"].ReadOnly = true;
				dataGridView1.Columns["Summa"].ReadOnly = true;
				dataGridView1.Columns["App_Not"].ReadOnly = true;
				while (mySqlDataReader.Read())
				{
					dataGridView1.Rows.Add(mySqlDataReader["ID_Book"].ToString(), mySqlDataReader["Account_number"].ToString(), mySqlDataReader["Data_p"].ToString(), mySqlDataReader["Moon"].ToString(), mySqlDataReader["Summa"].ToString(), mySqlDataReader["App_Not"].ToString());
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData2()
		{
			database db = new database();
			string string_command = "SELECT * FROM `owners`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView2.Columns.Add("ID_Book", "Номер членской книжки");
				dataGridView2.Columns.Add("FIO", "ФИО пользователя");
				dataGridView2.Columns.Add("Address", "Адресс");
				dataGridView2.Columns.Add("Year_birth", "Дата рождения");
				dataGridView2.Columns.Add("Year_entry", "Дата вступления");
				dataGridView2.Columns["ID_Book"].Width = 100;
				dataGridView2.Columns["ID_Book"].ReadOnly = true;
				dataGridView2.Columns["FIO"].ReadOnly = true;
				dataGridView2.Columns["Address"].ReadOnly = true;
				dataGridView2.Columns["Year_birth"].ReadOnly = true;
				dataGridView2.Columns["Year_entry"].ReadOnly = true;
				while (mySqlDataReader.Read())
				{
					dataGridView2.Rows.Add(mySqlDataReader["ID_Book"].ToString(), mySqlDataReader["FIO"].ToString(), mySqlDataReader["Address"].ToString(), mySqlDataReader["Year_birth"].ToString(), mySqlDataReader["Year_entry"].ToString());
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_update_Click(object sender, EventArgs e)
		{
			dataGridView1.Columns.Remove("ID_Book");
			dataGridView1.Columns.Remove("Account_number");
			dataGridView1.Columns.Remove("Data_p");
			dataGridView1.Columns.Remove("Moon");
			dataGridView1.Columns.Remove("Summa");
			dataGridView1.Columns.Remove("App_Not");
			loadData();
		}

		private void button_add_Click(object sender, EventArgs e)
		{
			database db = new database();
			string command_string = "INSERT INTO `payment` (Account_number, Data_p, Moon, Summa, App_Not) VALUES (@Num,@Data,@Moon,@Summ,@App_Not)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@Num", MySqlDbType.Int32).Value = textBox_num_acc.Text;
			mySqlCommand.Parameters.Add("@Data", MySqlDbType.DateTime).Value = dateTimePicker.Value;
			mySqlCommand.Parameters.Add("@Moon", MySqlDbType.VarChar).Value = textBox_moon.Text;
			mySqlCommand.Parameters.Add("@Summ", MySqlDbType.Int32).Value = textBox_summ.Text;
			mySqlCommand.Parameters.Add("@App_Not", MySqlDbType.Binary).Value = textBox_appNot.Text;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Оплата успешно добавлена", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_delete_Click(object sender, EventArgs e)
		{
			database db = new database();
			string command_string = "DELETE FROM `payment` WHERE Account_number=@Num";
			MySqlCommand sqlCommand = new MySqlCommand(command_string, db.GetConnection());
			sqlCommand.Parameters.Add("@Num", MySqlDbType.Int32).Value = textBox_delNum.Text;
			try
			{
				db.OpenConnection();
				if (sqlCommand.ExecuteNonQuery() >= 1)
				{
					MessageBox.Show("Оплата успешно удалена!", "Успешно!");
				}
				else
				{
					MessageBox.Show("Предмета с таким артикулом не существует.", "Ошибка!");
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных. Обратитесь к администратору системы.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button2_Click(object sender, EventArgs e)
		{
			unit_test();
			database db = new database();
			string command_string = "INSERT INTO `owners` (ID_Book, FIO, Address , Year_birth, Year_entry) VALUES (@id, @fio,@address,@year_birth,@year_empty)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = textBox_id.Text;
			mySqlCommand.Parameters.Add("@fio", MySqlDbType.VarChar).Value = textBox_FIO.Text;
			mySqlCommand.Parameters.Add("@address", MySqlDbType.VarChar).Value = textBox_address.Text;
			mySqlCommand.Parameters.Add("@year_birth", MySqlDbType.Date).Value = dateTimePicker_year.Value;
			mySqlCommand.Parameters.Add("@year_empty", MySqlDbType.Date).Value = dateTimePicker_empty.Value;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Владелец успешно добавлен", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			database db = new database();
			string command_string = "DELETE FROM `owners` WHERE ID_Book=@id_book";
			MySqlCommand sqlCommand = new MySqlCommand(command_string, db.GetConnection());
			sqlCommand.Parameters.Add("@id_book", MySqlDbType.Int32).Value = textBox_IDBook.Text;
			try
			{
				db.OpenConnection();
				if (sqlCommand.ExecuteNonQuery() >= 1)
				{
					MessageBox.Show("Предмет успешно удален!", "Успешно!");
				}
				else
				{
					MessageBox.Show("Предмета с таким артикулом не существует.", "Ошибка!");
				}
			}
			catch
			{
				MessageBox.Show("Нет подключения к базе данных. Обратитесь к администратору системы.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_upadte2_Click(object sender, EventArgs e)
		{
			dataGridView2.Columns.Remove("ID_Book");
			dataGridView2.Columns.Remove("FIO");
			dataGridView2.Columns.Remove("Address");
			dataGridView2.Columns.Remove("Year_birth");
			dataGridView2.Columns.Remove("Year_entry");
			loadData2();
		}

		[TestMethod]
		public void unit_test()
        {
			int id = 3;
			textBox_id.Text = id.ToString(); ;
			string FIO = "Иванов Иван Иванович";
			textBox_FIO.Text = FIO;
			string address = "12";
			textBox_address.Text = address;
			dateTimePicker_year.Value = new DateTime(2009, 3, 23);
			dateTimePicker_empty.Value = new DateTime(2019, 3, 23);

		}
		private void button_add4_Click(object sender, EventArgs e)
		{
			database db = new database();
			int id = 0;

			if (listBox2.SelectedItem == "Скай джоринг спринт")
				id = 1;
			if (listBox2.SelectedItem == "Скай джоринг полумарафон")
				id = 2;
			if (listBox2.SelectedItem == "Скай джоринг Пулка спринт")
				id = 3;
			if (listBox2.SelectedItem == "Пулка среднии дистанции")
				id = 4;
			if (listBox2.SelectedItem == "Драйленд кан кросс")
				id = 5;
			if (listBox2.SelectedItem == "Лыжи - спринт")
				id = 6;
			if (listBox2.SelectedItem == "Байк кросс")
				id = 7;
			if (listBox2.SelectedItem == "Байк кросс")
				id = 8;
			if (listBox2.SelectedItem == "Нарта - спринт")
				id = 9;
			if (listBox2.SelectedItem == "Кросс")
				id = 10;
			if (listBox2.SelectedItem == "Марафон")
				id = 11;
			if (listBox2.SelectedItem == "Супер марафон")
				id = 12;

			string command_string = "INSERT INTO `race` (ID_race, Data_r, Time_r) VALUES (@id,@datar,@timer)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
			mySqlCommand.Parameters.Add("@datar", MySqlDbType.Date).Value = dateTimePicker_r1.Value;
			mySqlCommand.Parameters.Add("@timer", MySqlDbType.Date).Value = dateTimePicker_r2.Value;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					MessageBox.Show("Заезд успешно добавлен", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData3()
		{
			database db = new database();
			string string_command = "select * from `race`,`type_race`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView3.Columns.Add("ID_race", "Номер гонки");
				dataGridView3.Columns.Add("Data_r", "Начало заезда");
				dataGridView3.Columns.Add("Time_r", "Время заезда");
				dataGridView3.Columns.Add("Titles", "Тип гонки");
				dataGridView3.Columns["ID_race"].Width = 100;
				dataGridView3.Columns["ID_race"].ReadOnly = true;
				dataGridView3.Columns["Data_r"].ReadOnly = true;
				dataGridView3.Columns["Time_r"].ReadOnly = true;
				dataGridView3.Columns["Titles"].ReadOnly = true;
				while (mySqlDataReader.Read())
				{
					dataGridView3.Rows.Add(mySqlDataReader["ID_race"].ToString(), mySqlDataReader["Data_r"].ToString(), mySqlDataReader["Time_r"].ToString(), mySqlDataReader["Titles"].ToString());
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_del2_Click(object sender, EventArgs e)
		{
			database db = new database();
			string command_string = "DELETE FROM `type_race` WHERE ID_race=@id";
			string command_string_1 = "DELETE FROM `race` WHERE ID_race=@id";
			MySqlCommand sqlCommand = new MySqlCommand(command_string, db.GetConnection());
			MySqlCommand sqlCommand_1 = new MySqlCommand(command_string_1, db.GetConnection());
			sqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = textBox_rid.Text;
			try
			{
				db.OpenConnection();
				if (sqlCommand.ExecuteNonQuery() >= 1)
				{
					MessageBox.Show("Заезд успешно удален!", "Успешно!");
				}
				else
				{
					MessageBox.Show("Заезда с таким артикулом не существует.", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				MessageBox.Show("Нет подключения к базе данных. Обратитесь к администратору системы.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_up3_Click(object sender, EventArgs e)
		{
			dataGridView3.Columns.Remove("ID_race");
			dataGridView3.Columns.Remove("Data_r");
			dataGridView3.Columns.Remove("Time_r");
			dataGridView3.Columns.Remove("Titles");
			loadData3();
		}

        private void button_addUser_Click(object sender, EventArgs e)
        {
			int role = 2;
			if (listBox1.SelectedItem == "Участник")
				role = 0;
			else if (listBox1.SelectedItem == "Кинолог")
				role = 1;
			else if (listBox1.SelectedItem == "Председатель")
				role = 2;

			database db = new database();
			string command_string = "INSERT INTO `users` (login, password, role) VALUES (@login,@password,@role)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@login", MySqlDbType.VarChar).Value = textBox_login.Text;
			mySqlCommand.Parameters.Add("@password", MySqlDbType.VarChar).Value = textBox_passwd.Text;
			mySqlCommand.Parameters.Add("@role", MySqlDbType.Int32).Value = role;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1 )
				{
					MessageBox.Show("Пользователь успешно добавлен", "Успешно");
				}
				else
				{
					MessageBox.Show("Что-то пошло не так!", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
				db.CloseConnection();
			}

		}

        private void button_delUser_Click(object sender, EventArgs e)
        {
			database db = new database();
			string command_string = "DELETE FROM `users` WHERE user_id=@id";
			MySqlCommand sqlCommand = new MySqlCommand(command_string, db.GetConnection());
			sqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = textBox_uid.Text;
			try
			{
				db.OpenConnection();
				if (sqlCommand.ExecuteNonQuery() >= 1)
				{
					MessageBox.Show("Пользователь успешно удален!", "Успешно!");
				}
				else
				{
					MessageBox.Show("Пользователя с таким артикулом не существует.", "Ошибка!");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
				MessageBox.Show("Нет подключения к базе данных. Обратитесь к администратору системы.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		public void loadData4()
		{
			database db = new database();
			string string_command = "select * from `users`;";
			MySqlCommand mySqlCommand = new MySqlCommand(string_command, db.GetConnection());
			try
			{
				db.OpenConnection();
				MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
				dataGridView4.Columns.Add("user_id", "Идентификатор пользователя");
				dataGridView4.Columns.Add("login", "Имя пользователя");
				dataGridView4.Columns.Add("password", "Пароль");
				dataGridView4.Columns.Add("role", "Роль");
				dataGridView4.Columns["user_id"].Width = 100;
				dataGridView4.Columns["user_id"].ReadOnly = true;
				dataGridView4.Columns["login"].ReadOnly = true;
				dataGridView4.Columns["password"].ReadOnly = true;
				dataGridView4.Columns["role"].ReadOnly = true;
				while (mySqlDataReader.Read())
				{
					dataGridView4.Rows.Add(mySqlDataReader["user_id"].ToString(), mySqlDataReader["login"].ToString(), mySqlDataReader["password"].ToString(), mySqlDataReader["role"].ToString());
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Нет подключения к базе данных, обратитесь к системному администратору.", "Ошибка!");
			}
			finally
			{
				db.CloseConnection();
			}
		}

		private void button_up4_Click(object sender, EventArgs e)
        {
			dataGridView4.Columns.Remove("user_id");
			dataGridView4.Columns.Remove("login");
			dataGridView4.Columns.Remove("password");
			dataGridView4.Columns.Remove("role");
			loadData4();
		}
    }
}
