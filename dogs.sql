-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: dogs
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `breed`
--

DROP TABLE IF EXISTS `breed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `breed` (
  `ID_breed` int NOT NULL,
  `Title_breed` int NOT NULL,
  PRIMARY KEY (`ID_breed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dog_rewards`
--

DROP TABLE IF EXISTS `dog_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dog_rewards` (
  `ID_mark_dog` int DEFAULT NULL,
  `ID_totaly` int DEFAULT NULL,
  `ID_race` int DEFAULT NULL,
  `Награда` varchar(18) DEFAULT NULL,
  KEY `ID_mark_dog` (`ID_mark_dog`),
  KEY `ID_totaly` (`ID_totaly`),
  KEY `ID_race` (`ID_race`),
  CONSTRAINT `dog_rewards_ibfk_1` FOREIGN KEY (`ID_mark_dog`) REFERENCES `dogs` (`ID_mark_dog`),
  CONSTRAINT `dog_rewards_ibfk_2` FOREIGN KEY (`ID_totaly`) REFERENCES `totaly` (`ID_totaly`),
  CONSTRAINT `dog_rewards_ibfk_3` FOREIGN KEY (`ID_race`) REFERENCES `race` (`ID_race`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dogs`
--

DROP TABLE IF EXISTS `dogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dogs` (
  `ID_book` int DEFAULT NULL,
  `ID_breed` int DEFAULT NULL,
  `ID_pass` int DEFAULT NULL,
  `ID_mark_dog` int NOT NULL,
  `Breeder` varchar(18) DEFAULT NULL,
  `Name_dog` varchar(18) NOT NULL,
  `Year_dog` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_mark_dog`),
  KEY `ID_book` (`ID_book`),
  KEY `ID_breed` (`ID_breed`),
  KEY `ID_pass` (`ID_pass`),
  CONSTRAINT `dogs_ibfk_1` FOREIGN KEY (`ID_book`) REFERENCES `owners` (`ID_book`),
  CONSTRAINT `dogs_ibfk_2` FOREIGN KEY (`ID_breed`) REFERENCES `breed` (`ID_breed`),
  CONSTRAINT `dogs_ibfk_3` FOREIGN KEY (`ID_pass`) REFERENCES `med_passport_dog` (`ID_pass`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `in_races`
--

DROP TABLE IF EXISTS `in_races`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `in_races` (
  `ID_mark_dog` int DEFAULT NULL,
  `ID_team` int DEFAULT NULL,
  `ID_race` int DEFAULT NULL,
  `ID_sied` int DEFAULT NULL,
  `Number_r` int NOT NULL,
  KEY `ID_mark_dog` (`ID_mark_dog`),
  KEY `ID_team` (`ID_team`),
  KEY `ID_race` (`ID_race`),
  KEY `ID_sied` (`ID_sied`),
  CONSTRAINT `in_races_ibfk_1` FOREIGN KEY (`ID_mark_dog`) REFERENCES `dogs` (`ID_mark_dog`),
  CONSTRAINT `in_races_ibfk_2` FOREIGN KEY (`ID_team`) REFERENCES `type_of_team` (`ID_team`),
  CONSTRAINT `in_races_ibfk_3` FOREIGN KEY (`ID_race`) REFERENCES `race` (`ID_race`),
  CONSTRAINT `in_races_ibfk_4` FOREIGN KEY (`ID_sied`) REFERENCES `type_sied` (`ID_sied`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `med_passport_dog`
--

DROP TABLE IF EXISTS `med_passport_dog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `med_passport_dog` (
  `ID_pass` int NOT NULL,
  `ID_string` int DEFAULT NULL,
  PRIMARY KEY (`ID_pass`),
  KEY `ID_string` (`ID_string`),
  CONSTRAINT `med_passport_dog_ibfk_1` FOREIGN KEY (`ID_string`) REFERENCES `type_string` (`ID_string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `owners`
--

DROP TABLE IF EXISTS `owners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `owners` (
  `ID_book` int NOT NULL,
  `FIO` varchar(30) NOT NULL,
  `Address` varchar(4) DEFAULT NULL,
  `Year_birth` datetime NOT NULL,
  `Year_entry` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_book`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `ID_book` int DEFAULT NULL,
  `Account_number` int NOT NULL,
  `Data_p` datetime NOT NULL,
  `Moon` varchar(10) NOT NULL,
  `Summa` int NOT NULL,
  `App_Not` binary(2) NOT NULL,
  KEY `ID_book` (`ID_book`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`ID_book`) REFERENCES `owners` (`ID_book`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `race`
--

DROP TABLE IF EXISTS `race`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `race` (
  `ID_race` int NOT NULL,
  `Data_r` datetime DEFAULT NULL,
  `Time_r` datetime DEFAULT NULL,
  PRIMARY KEY (`ID_race`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `totaly`
--

DROP TABLE IF EXISTS `totaly`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `totaly` (
  `ID_totaly` int NOT NULL,
  `Титул` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`ID_totaly`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_of_team`
--

DROP TABLE IF EXISTS `type_of_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_of_team` (
  `ID_team` int NOT NULL,
  `Quantity` int DEFAULT NULL,
  PRIMARY KEY (`ID_team`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_race`
--

DROP TABLE IF EXISTS `type_race`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_race` (
  `ID_race` int DEFAULT NULL,
  `Titles` varchar(18) DEFAULT NULL,
  KEY `ID_race` (`ID_race`),
  CONSTRAINT `type_race_ibfk_1` FOREIGN KEY (`ID_race`) REFERENCES `race` (`ID_race`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_sied`
--

DROP TABLE IF EXISTS `type_sied`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_sied` (
  `ID_sied` int NOT NULL,
  `Title` varchar(18) DEFAULT NULL,
  `Netto` int DEFAULT NULL,
  PRIMARY KEY (`ID_sied`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `type_string`
--

DROP TABLE IF EXISTS `type_string`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_string` (
  `ID_string` int NOT NULL,
  `String` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`ID_string`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `role` int DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-15 23:53:06
