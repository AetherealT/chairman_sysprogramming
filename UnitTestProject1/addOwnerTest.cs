﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MySql.Data.MySqlClient;

namespace UnitTestProject1
{
    [TestClass]
    public class addOwnerTest
    {
        [TestMethod]
        public void TestMethod1()
        {
			int id = 5;
			string fio = "Иванов Иван Иванович";
			string address = "1241";
			database db = new database();
			DateTime date_year = new DateTime();
			date_year = new DateTime(2009, 3, 23);
			DateTime date_empty = new DateTime();
			date_empty = new DateTime(2009, 3, 23);

			string command_string = "INSERT INTO `owners` (ID_Book, FIO, Address , Year_birth, Year_entry) VALUES (@id, @fio,@address,@year_birth,@year_empty)";
			MySqlCommand mySqlCommand = new MySqlCommand(command_string, db.GetConnection());
			mySqlCommand.Parameters.Add("@id", MySqlDbType.Int32).Value = id;
			mySqlCommand.Parameters.Add("@fio", MySqlDbType.VarChar).Value = fio;
			mySqlCommand.Parameters.Add("@address", MySqlDbType.VarChar).Value = address;
			mySqlCommand.Parameters.Add("@year_birth", MySqlDbType.Date).Value = date_year;
			mySqlCommand.Parameters.Add("@year_empty", MySqlDbType.Date).Value = date_empty;
			try
			{
				db.OpenConnection();
				if (mySqlCommand.ExecuteNonQuery() == 1)
				{
					
				}
				else
				{
					
				}
			}
			catch (Exception ex)
			{
				
			}
			finally
			{
				db.CloseConnection();
			}
		}
    }
}
