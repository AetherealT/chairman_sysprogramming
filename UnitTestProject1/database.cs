﻿using MySql.Data.MySqlClient;

namespace UnitTestProject1
{
    class database
    {
        MySqlConnection connection = new MySqlConnection("server=localhost;port=3306;username=aether;password=1exc2512;database=dogs");

        public void OpenConnection()
        {
            if(connection.State == System.Data.ConnectionState.Closed)
            {
                connection.Open();
            }
        }

        public void CloseConnection()
        {
            if(connection.State == System.Data.ConnectionState.Open)
            {
                connection.Close();
            }
        }

        public MySqlConnection GetConnection() => connection;
    }
}
